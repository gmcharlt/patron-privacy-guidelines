# Guidelines for protecting library patron privacy

## Resources in this repository

* [Guidelines for protecting patron privacy when making and responding to technical support requests](technical-support-interactions.md)

## External resources

* [Library Privacy Guidelines by the American Library Association](http://www.ala.org/advocacy/privacy/guidelines)
  * [Library Privacy Checklists](http://www.ala.org/advocacy/privacy/checklists)
