# Guidelines for protecting patron privacy when making and responding to technical support requests

Libraries and library staff are obligated to protect the privacy of the people
who use the library. This includes keeping confidential what resources library
patrons request and use. It also includes protecting information that may
identify patrons to others or expose them to identity theft.

The software and databases that libraries and patrons use often store
information about patron use of the library. Software inevitably has bugs and
other problems that may require that library staff seek help from a support
provider. Sometimes, in order to communicate clearly about an issue so that it
can be fixed, there may be no alternative to referring to information about
specific patrons. To help protect patron privacy in such a situation, here
follows guidelines for staff at libraries and support providers when handling
patron information that may need to be communicated during a technical support
interaction.

## Guidelines for front-line staff

* Avoid referring to specific patron records at all whenever possible. Only
  include enough information to allow the person handling it to fix the problem
  being reported.
* If a problem that manifests for a specific patron or group of patrons can be
  readily reproduced on a test system or using a test record, refer to that test
  record or system first when making the support request, moving on to "real"
  patron records only if necessary to solve the problem.
* Do not refer to specific patrons by name or other personal identifiers such as
  email address or student ID; refer to them by an internal database ID, barcode,
  or RFID tag value. Avoid using identifiers that are part of the credentials
  that patrons use to authenticate to the system.
  * In the U.S., if the patron's Social Security Number is stored in the library's
    system, under no circumstances should it be given out.
* If it is necessary to refer to details of a patron's library activity, avoid
  referring to any resources they have checked out or requested by title;
  instead, refer to those resources by more opaque identifiers such as internal
  database ID or barcode.  If a relevant interaction can be referred to by
  timestamp (e.g., the checkout to patron 123 made at 2017-07-18 09:27), use that
  instead.
* If it is necessary to communicate a list of patron records, avoiding do so in
  ways that communicate personally identifiable information (PII), and use
  encrypted channels. For example, rather than email a spreadsheet of barcodes of
  patrons to request a batch change, upload it to a secure file drop.
*  Tidy up any personal notes made during the course of handling the support
  request to redact any incidental references to PII.
* Shred any paper generated in the process of documenting, troubleshooting, or
  resolving the process - including handwritten notes or printouts.

It should be noted that one of the intentions for this section is to focus on
guidelines that front-line staff can implement on their own without necessarily
requiring buy-in by management.

## Examples

### How to report an issue while protecting patron privacy

*When patrons check out DVDs, the loan period is 21 days when it should be
seven. I'm able to make this happen consistently with one of my test records;
please take a look at record fake-adult-patron-123 and its checkout of item
1552746664.*

*The system says that patron 489763 was sent two hold available notice emails
around 10:30 a.m. on Thursday, but the patron says that they never got them.
Could you find out what happened to those emails?*

### How not to do it

*Jane Smith is trying to place a hold on "l8r g8r", but it's not letting her.
Could you tell me why?*

This discloses that a specific person is try to use a specific book (which
happens to be on ALA's list of top 100 banned/challenged books).

*Here's a screenshot of Joey Dale's patron account. As you can see, zir fines
don't add up to the amount due in the header. What's up with that?*

Again, this discloses a specific patron's financial obligation to the library.
Depending on the system, the screenshot may include additional information
about the patron that is irrelevant to the support request.

## Additional guidelines for supervisors and middle management

* Ensure that staff have adequate training on these guidelines and any policies
  and procedures adopted by the organization to implement them.
* Given that it takes only one party in a support interaction to disclose PII, be
  prepared to help assist staff with communicating these guidelines to support
  partners.

## Additional guidelines for management and administration

* Devise policies and procedures for implementing these guidelines, being
  sensitive to the consideration that as with other computer security policies, a
  hamfisted approach can lead to the intent of those policies being subverted in
  the name of "getting work done".

## Additional guidelines for ticket systems

* Despite the guidelines listed above and processes the implement them, it is
  inevitable that PII will occasionally and inadvertently slip into discussions
  about a support request. Consequently, ticket systems should support readily
  and permanently redacting confidential information.
  * Corollary for bug/issue tracking systems for open source systems: the ability
    to redact information from bug reports can be important for exactly the same
    reason.
* There should be a way to mark a ticket so that the full content of a ticket
  update are not sent via email.

## End matter

This document is copyright 2017 by Galen Charlton and others and is available
under a CC-BY-SA license. 

*NOTE: if you make a substantial contribution to this document, i.e., beyond
just a typo fix, please feel free to also add your name to the section below
for attribution.*

### Contributors

* Galen Charlton
* Lisa Hinchliffe

### Acknowledgments

We would like to thank the following people who provided feedback on this document:

* Marlene Harris
* Dorothea Salo
